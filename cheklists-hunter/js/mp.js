var csvArr = [];
var csvArrClean = {};
var nameArray = [];
var myId;
if ($('.b-item-add-mini').length > 0) {
    taskIdRaw = document.location.pathname;
    var myRe = /\/task\/(\d{4,7})\//;
    var taskId = myRe.exec(taskIdRaw);
    taskIdN = taskId[1];
    $(document).on('click', '.chkl-link', function () {
        $(this).closest('#checklist-hunter-container').toggleClass('visible');
    });
    $(document).on('click', function (e) {
        if (!$(e.target).is('#checklist-hunter-container') && $(e.target).closest('#checklist-hunter-container').length == 0 && $('#checklist-hunter-container').hasClass('visible')) {
            $('#checklist-hunter-container').removeClass('visible');
        }
    });
    $(document)
            .on('mouseenter dragover', '.js-file-input-container', function (e) {
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            })
            .on('mouseleave dragleave', '.js-file-input-container', function () {
                $(this).toggleClass('hover');
            })
    chrome.extension.sendRequest({key: 'key'}, function (response) {
        target = 'https://mgtsk.ru/acceptor/';
        key = response.key;
        $.ajax({
            url: target + 'timeCount',
            type: 'POST',
            data: {key: key},
            success: function (response) {
                if (response.info.adress == document.location.host) {
                    console.log('Hi, I\'m MGTSK Cheklists Hunter!');

                    $('.b-item-add-mini').append("<div class=\"checklist-hunter-add chkl-wrapper\" id=\"checklist-hunter-container\"></div>");
                    /*вот зачем я это делаю, если можно просто включить хтмл?*/
                    $('#checklist-hunter-container').append('<a class="chkl-link" />');
                    $('.chkl-link').attr('href', '#').html('<i class="glyphicon glyphicon-plus"/>');
                    $('#checklist-hunter-container').append('<div class="chkl-cont" />');

                    $('.chkl-cont').append('<div class="chkl-list-wrapper" id="chkl-list-wrapper" />');

                    $('.chkl-cont').append('<div id="js-file-drag" class="js-overlay js-file-input-container mgt-file-drag" style="display: block;" />');
                    $('#js-file-drag').append('<div class="mgt-file-drag-input mgt-bordered">Перетащите файл сюда</div>');
                    $('#js-file-drag').append('<input type="file" id="mp-get-file" name="mp-get-file" accept=".csv" class="js-upload-file" style="display: inline-block;"/>');

                    $('.chkl-cont').append('<div id="js-loader" class="mgt-file-drag-input js-overlay" />');
                    $('#js-loader').hide();
                    $('#js-loader').append('<span id="js-task-state" data-state-upload="Файлы загружаются" data-state-process="Информация обрабатывается" data-state-save="Задача отправляется"/>');
                    $('#js-loader #js-task-state').append('<div class="mgt-loader" />');
                    $('#js-loader .mgt-loader').append('<div class="mgt-loader-animation" />');
                    $('#js-loader .mgt-loader-animation').append('<div class="mgt-loader-animation-item mgt-loader-animation-item-n1" />');
                    $('#js-loader .mgt-loader-animation').append('<div class="mgt-loader-animation-item mgt-loader-animation-item-n2" />');
                    $('#js-loader .mgt-loader-animation').append('<div class="mgt-loader-animation-item mgt-loader-animation-item-n3" />');

                    $('.chkl-cont').append('<a href="http://mgtsk.ru" class="chkl-link-mgtsk" target="_blank"/>');
                    $('#checklist-hunter-container').on('click', '.input-plus-title', function () {
                        $('.chkl-csvfile').toggleClass('show');
                    });
                }
            }
        });

        $(document).on('change', '#mp-get-file', function (event) {
            event.preventDefault;
            csvArrClean = [];
            nameArray = [];
            //            console.log('Type: ' + event.target.files[0].type);
            //      if(event.target.files[0].type !== '') {
            //          return false;
//      }
            var inputType = "local";
            var stepped = 0, rowCount = 0, errorCount = 0, firstError;
            var start, end;
            var firstRun = true;
            var maxUnparseLength = 10000;
            var config = {
                delimiter: '|',
                //      header: $('#header').prop('checked'),
                //      dynamicTyping: $('#dynamicTyping').prop('checked'),
                //      skipEmptyLines: $('#skipEmptyLines').prop('checked'),
                //      preview: parseInt($('#preview').val() || 0),
                //      step: $('#stream').prop('checked') ? stepFn : undefined,
                //      encoding: $('#encoding').val(),
                //      worker: $('#worker').prop('checked'),
                //      comments: $('#comments').val(),
                complete: completeFn,
                error: errorFn,
                //      download: inputType == "remote"
            }
            $('#mp-get-file').parse({
                config: config,
                before: function (file, inputElem) {
                    start = now();
                    //                    console.log("Parsing file...", file);
                    $('#js-file-drag').hide();
                    $('#chkl-list-wrapper').empty();
                    $('#js-loader').show();
                },
                error: function (err, file) {
                    console.log("ERROR:", err, file);
                    firstError = firstError || err;
                    errorCount++;
                },
                complete: function () {
                    end = now();
                    $('#js-loader').hide();
                    if (csvArrClean.length > 0) {
                        $('#chkl-list-wrapper').append('<ul class="list chkl-list" />');
                        $(csvArrClean).each(function (i, el) {
                            $('#chkl-list-wrapper .chkl-list').append('<li class="chkl-list-item" />');
                            $('#chkl-list-wrapper .chkl-list li:last').append('<div class="chkl-list-item-title">' + el.name + '</div>');
                            if (el.desc !== undefined) {
                                $('#chkl-list-wrapper .chkl-list li:last').append('<div class="chkl-list-item-desc">' + el.desc + '</div>');
                            }
                        });
                        $('.chkl-cont').append('<a href="#" class="chkl-link-settask" id="mp-set-tasks"/>');
                        $('.chkl-link-settask').append('<i class="glyphicon glyphicon-chevron-down"/>');
                    }
                    $('#js-file-drag').show();
                }
            });
        });
        $(document).on('click', '#mp-set-tasks', function (event) {
            event.preventDefault();
            $('.mgt-file-link').remove();

            $('#js-file-drag').hide();
            $('#js-set-task').hide();
            $('#js-loader').show();
            var defers = [];
            var defer;
            csvArrClean.forEach(function (el, i) {
                name = el.name;
                desc = el.desc;
                defer = $.ajax({
                    url: target + 'setChecklist',
                    type: 'POST',
                    data: {key: key, data: {
                            SubjectType: 'task',
                            SubjectId: taskIdN,
                            Model: {
                                Title: name,
                            }
                        }
                    },
                    success: function (response) {
                        //                        console.log(response);
//                        if (response.status.code === 'ok') {

//                        }
                    }
                });
                defers.push(defer);
            });
            $('#chkl-list-wrapper').empty();
            $.when.apply(window, defers).done(function () {
//                console.log('i\'m send!');
                successSend();
                $.ajax({
                    url: target + 'getChecklists',
                    type: 'POST',
                    data: {key: key, data: {
                            OnlyActual: true,
                            taskId: taskIdN,
                        }
                    },
                    success: function (response) {
                        if (response.status.code == 'ok') {
                            myId = parseInt(response.info.employee_id);
                            var defers = [];
                            var defer;
                            var now = new Date();
                            cheklistsItem = response.data;
                            cheklistsItem.forEach(function (el, i) {
                                count = nameArray.indexOf(el.Title)
                                if (csvArrClean[count].name != undefined) {
                                    defer = $.ajax({
                                        url: target + 'updEvent',
                                        type: 'POST',
                                        data: {key: key, data: {
                                                Id: el.Id,
                                                Model: {
                                                    Name: el.Title,
                                                    Description: csvArrClean[count].desc,
                                                    Responsible: myId,
                                                    From: now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes()
                                                }
                                            }
                                        },
                                        success: function (response) {
                                            if (response.status.code === 'ok') {
                                                //$('.project-statement').append('<div data-id="' + response.data.event.Id + '"><h4>' + response.data.event.Name + '</h4>' + response.data.event.Description + '</div>');
                                            }
                                        }
                                    });
                                    defers.push(defer);
                                }
                            });
                            $.when.apply(window, defers).done(function () {
                                $('#js-file-drag').show();
                                $('#js-loader').hide();
                                location.reload();
                            });
                        }
                    }});
            });
        });
    });
    $(document).on('click', '.reloadme', function () {
        location.reload();
    });
}
function now() {
    return typeof window.performance !== 'undefined'
            ? window.performance.now()
            : 0;
}
function printStats(msg) {
//    if (msg)
    //        console.log(msg);
}
function completeFn(results) {
    end = now();
    if (results && results.errors)
    {
        if (results.errors)
        {
            errorCount = results.errors.length;
            firstError = results.errors[0];
        }
        if (results.data && results.data.length > 0) {
            rowCount = results.data.length;
            r = results.data;
            csvArr = r;
            r.forEach(function (el, i) {
                name = el[0];
                desc = el[1];
                csvArrClean[i] = {};
                csvArrClean[i].name = name;
                csvArrClean[i].desc = desc;
                nameArray.push(name);
            });
        }
    }

//    console.log("Results:", results);
}

function errorFn(err, file) {
    end = now();
    console.log("ERROR:", err, file);
}
function beforeSend() {
    $('.mgt').removeClass('mgt-drag-hidden');
}
function successSend() {
    $('.mgt').addClass('mgt-drag-hidden');
}