function install_notice() {
    if (localStorage.getItem('mgtskKey'))
        return;
    if (chrome.runtime.openOptionsPage !== undefined) {
        chrome.runtime.openOptionsPage();
    } else {
        chrome.tabs.create({'url': 'chrome-extension://' + chrome.runtime.id + '/html/options.html'});
    }
}
install_notice();
// Background Page
chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
  if (request.key) {
    key = localStorage['mgtskKey'];
    sendResponse({key: key});
  } else {
    sendResponse({});
  }
});