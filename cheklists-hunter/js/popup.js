var csvArr = [];
var csvArrClean = {};
target = 'http://mgtsk.ru/acceptor/';
key = localStorage['mgtskKey'];
$(document).ready(function () {
  var jqxhr1 = $.ajax({
    url: target + 'getTasks',
    type: 'POST',
    data: {key: key, data: {'Status': 'actual', 'Folder': 'responsible'}},
    success:
            function (response) {
              $('.mgt').addClass('mgt-drag-hidden');
              console.log(JSON.stringify(response));
              if (response.status.code === 'ok') {
                console.log(response);
                var combo = $("<select></select>").attr("id", "mp-get-tasks").attr("name", "mp-get-tasks").attr('class', 'mgt-input');
                combo.append("<option selected disabled>Мои задачи</option>");
                tasks = response.data;
                tasks.forEach(function (el, i) {
                  combo.append("<option value=" + el.Id + ">" + el.Name + "</option>");
                });
                $('#mp-get-tasks-wr')
                        .html('')
                        .append(combo);
              }
            },
    error:
            function (data) {
              console.log(data);
            }
  });
  var jqxhr2 = $.ajax({
    url: target + 'getEmployees',
    type: 'POST',
    data: {key: key},
    success:
            function (response) {
              if (response.status.code === 'ok') {
                console.log(response);
                employees = response.data;
                var combo = $("<select></select>").attr("id", "mp-get-users").attr("name", "mp-get-users").attr('class', 'mgt-input');
                combo.append("<option disabled>Пользователи</option>");
                employees.forEach(function (el, i) {
                  if (response.info.employee_id == el.Id) {
                    combo.append("<option value=" + el.Id + " selected>" + el.Name + "</option>");
                  } else {
                    combo.append("<option value=" + el.Id + ">" + el.Name + "</option>");
                  }
                });
                $('#mp-get-users-wr')
                        .html('')
                        .append(combo);
              }
            },
    error:
            function (data) {
              console.log(data);
            }
  });
  beforeSend();
  $.when(jqxhr1, jqxhr2)
          .done(function (jqxhr1, jqxhr2) {
            successSend();
          });
  $(document).on('change', '#mp-get-file', function (e) {
    e.preventDefault;
    var inputType = "local";
    var stepped = 0, rowCount = 0, errorCount = 0, firstError;
    var start, end;
    var firstRun = true;
    var maxUnparseLength = 10000;
    var config = {
      delimiter: '|',
//      header: $('#header').prop('checked'),
//      dynamicTyping: $('#dynamicTyping').prop('checked'),
//      skipEmptyLines: $('#skipEmptyLines').prop('checked'),
//      preview: parseInt($('#preview').val() || 0),
//      step: $('#stream').prop('checked') ? stepFn : undefined,
//      encoding: $('#encoding').val(),
//      worker: $('#worker').prop('checked'),
//      comments: $('#comments').val(),
      complete: completeFn,
      error: errorFn,
//      download: inputType == "remote"
    }
    $('#mp-get-file').parse({
      config: config,
      before: function (file, inputElem) {
        start = now();
        console.log("Parsing file...", file);
      },
      error: function (err, file) {
        console.log("ERROR:", err, file);
        firstError = firstError || err;
        errorCount++;
      },
      complete: function () {
        end = now();
      }
    });
  });
  $(document).on('click', '#mp-set-tasks', function (event) {
    event.preventDefault();
    $('#mp-get-tasks').removeClass('mgt-input-error');
    if ($('#mp-get-tasks').val() == null) {
      $('#mp-get-tasks').addClass('mgt-input-error');
      return false;
    }
    var defers = [];
    var defer;
    csvArr.forEach(function (el, i) {
      name = el[0];
      desc = el[1];
      defer = $.ajax({
        url: target + 'setChecklist',
        type: 'POST',
        data: {key: key, data: {
            SubjectType: 'task',
            SubjectId: parseInt($('#mp-get-tasks').val()),
            Model: {
              Title: name,
            }
          }
        },
        success: function (response) {
          if (response.status.code === 'ok') {
          }
        }
      });
      defers.push(defer);
    });
    beforeSend();
    $('.project-statement').empty();
    $.when.apply(window, defers).done(function () {
//      successSend();
      $.ajax({
        url: target + 'getChecklists',
        type: 'POST',
        data: {key: key, data: {
            OnlyActual: true,
            taskId: parseInt($('#mp-get-tasks').val()),
          }
        },
        success: function (response) {
          if (response.status.code == 'ok') {
            var defers = [];
            var defer;
            var now = new Date();
//            fromD = d.getHours() + 2;
            cheklistsItem = response.data;
            cheklistsItem.forEach(function (el, i) {
//              $('.onetask:contains("' + el.Title + '")').attr('data-id', el.Id);
//              console.log(el);
              if (csvArrClean[el.Title] != undefined) {
                defer = $.ajax({
                  url: target + 'updEvent',
                  type: 'POST',
                  data: {key: key, data: {
                      Id: el.Id,
                      Model: {
                        Name: el.Title,
                        Description: csvArrClean[el.Title],
                        Responsible: parseInt($('#mp-get-users').val()),
                        From: now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes()
                      }
                    }
                  },
                  success: function (response) {
                    if (response.status.code === 'ok') {
                      $('.project-statement').append('<div data-id="' + response.data.event.Id + '"><h4>' + response.data.event.Name + '</h4>' + response.data.event.Description + '</div>');
                    }
                  }
                });
                defers.push(defer);
              }
            });
            $.when.apply(window, defers).done(function () {
              successSend();
            });
          }
        }
      });
    });
  });
});
function now() {
  return typeof window.performance !== 'undefined'
          ? window.performance.now()
          : 0;
}
function printStats(msg) {
  if (msg)
    console.log(msg);
}
function completeFn(results) {
  end = now();
  if (results && results.errors)
  {
    if (results.errors)
    {
      errorCount = results.errors.length;
      firstError = results.errors[0];
    }
    if (results.data && results.data.length > 0) {
      rowCount = results.data.length;
      r = results.data;
      csvArr = r;
      r.forEach(function (el, i) {
        name = el[0];
        desc = el[1];
        csvArrClean[name] = desc;
        $('.project-statement').append('<h4>' + el[0] + '</h4>')
        $('.project-statement').append('<div>' + el[1] + '</div>')
      });
      $('#mp-set-tasks-wr')
              .empty()
              .append('<input type="submit" id="mp-set-tasks" class="mgt-button" value="Поставить задачи (' + csvArr.length + ')"/>');
    }

  }

  console.log("Results:", results);
}

function errorFn(err, file) {
  end = now();
  console.log("ERROR:", err, file);
}
function beforeSend() {
  $('.mgt').removeClass('mgt-drag-hidden');
}
function successSend() {
  $('.mgt').addClass('mgt-drag-hidden');
}